import torch
import torch.nn as nn
import torch.optim as optim
import matplotlib.pyplot as plt
from sklearn.datasets import make_blobs

# Generazione di dati di esempio
n_samples = 300
n_features = 2
n_clusters = 3
x, _ = make_blobs(n_samples=n_samples, n_features=n_features, centers=n_clusters, random_state=42)
x = torch.tensor(x, dtype=torch.float32)

# Visualizzazione dei dati di esempio
plt.scatter(x[:, 0], x[:, 1])
plt.xlabel('X1')
plt.ylabel('X2')
plt.show()

# Definizione del modello K-Means
class KMeans(nn.Module):
    def __init__(self, k, n_features):
        super(KMeans, self).__init__()
        self.centers = nn.Parameter(torch.randn(k, n_features))
        
    def forward(self, x):
        distances = torch.cdist(x, self.centers)
        return torch.argmin(distances, dim=1)

k = 3
model = KMeans(k, n_features)
optimizer = optim.Adam(model.parameters(), lr=0.1)

# Training del modello
num_epochs = 1000
for epoch in range(num_epochs):
    optimizer.zero_grad()
    cluster_indices = model(x)
    loss = (x - model.centers[cluster_indices]).pow(2).mean()
    loss.backward()
    optimizer.step()

    if (epoch+1) % 100 == 0:
        print(f'Epoch [{epoch+1}/{num_epochs}], Loss: {loss.item():.4f}')

# Visualizzazione dei centroidi e dei cluster
with torch.no_grad():
    cluster_indices = model(x)
    plt.scatter(x[:, 0], x[:, 1], c=cluster_indices, cmap='viridis')
    plt.scatter(model.centers[:, 0].numpy(), model.centers[:, 1].numpy(), s=300, c='red', marker='X')
    plt.xlabel('X1')
    plt.ylabel('X2')
    plt.show()

