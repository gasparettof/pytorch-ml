import torch
import torch.nn as nn
import torch.optim as optim
import matplotlib.pyplot as plt
from sklearn.datasets import make_moons

# Generazione dei dati di esempio
x, y = make_moons(n_samples=1000, noise=0.2, random_state=42)
x = torch.tensor(x, dtype=torch.float32)
y = torch.tensor(y, dtype=torch.float32).unsqueeze(1)

# Visualizzazione dei dati di esempio
plt.scatter(x[:, 0], x[:, 1], c=y.squeeze().numpy(), cmap='bwr')
plt.xlabel('X1')
plt.ylabel('X2')
plt.show()

# Definizione del modello di classificazione
class ClassificationModel(nn.Module):
    def __init__(self):
        super(ClassificationModel, self).__init__()
        self.layer1 = nn.Linear(2, 10)
        self.layer2 = nn.Linear(10, 1)
    
    def forward(self, x):
        x = torch.relu(self.layer1(x))
        x = torch.sigmoid(self.layer2(x))
        return x

model = ClassificationModel()

# Definizione della loss function e dell'ottimizzatore
criterion = nn.BCELoss()
optimizer = optim.Adam(model.parameters(), lr=0.01)

# Training del modello
num_epochs = 1000
for epoch in range(num_epochs):
    model.train()
    optimizer.zero_grad()
    outputs = model(x)
    loss = criterion(outputs, y)
    loss.backward()
    optimizer.step()

    if (epoch+1) % 100 == 0:
        print(f'Epoch [{epoch+1}/{num_epochs}], Loss: {loss.item():.4f}')

# Visualizzazione delle predizioni del modello (utilizzeremo una griglia per mostrare la decision boundary)
import numpy as np

xx, yy = np.mgrid[-1.5:2.5:.01, -1.0:1.5:.01]
grid = torch.tensor(np.c_[xx.ravel(), yy.ravel()], dtype=torch.float32)
predicted_grid = model(grid).detach().numpy().reshape(xx.shape)

plt.contourf(xx, yy, predicted_grid, levels=[0, 0.5, 1], alpha=0.5, cmap='bwr')
plt.scatter(x[:, 0], x[:, 1], c=y.squeeze().numpy(), cmap='bwr', edgecolor='k')
plt.xlabel('X1')
plt.ylabel('X2')
plt.show()

