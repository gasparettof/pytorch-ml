# Machine Learning with PyTorch

This repository contains example scripts for various machine learning tasks using PyTorch. The scripts demonstrate Linear Regression, Logistic Regression, Classification, and Clustering using K-Means. Each script includes data generation, model training, and visualization of results.

## Scripts

1. [Linear Regression](#linear-regression)
2. [Logistic Regression](#logistic-regression)
3. [Classification](#classification)
4. [K-Means Clustering](#k-means-clustering)

---

### Linear Regression

**Script**: `linear_regression.py`

This script demonstrates simple linear regression using PyTorch. The objective is to predict a continuous output based on a linear relationship with the input.

- **Data generation**: The data is generated using the equation `y = 2x + 1` with added noise.
- **Model**: A simple linear regression model with a single input and output feature.
- **Training**: The model is trained using Mean Squared Error (MSE) loss and Stochastic Gradient Descent (SGD).
- **Visualization**: The script plots the original data points and the fitted regression line.

**Results**:
- The scatter plot shows the original data points.
- The red line represents the fitted linear regression model, illustrating how well the model has captured the underlying linear relationship between the input and output.

---

### Logistic Regression

**Script**: `logistic_regression.py`

This script demonstrates logistic regression using PyTorch. The objective is to predict a binary outcome based on input features.

- **Data generation**: Binary classification data is generated using a linear function with added noise.
- **Model**: A logistic regression model with a single input feature and sigmoid activation.
- **Training**: The model is trained using Binary Cross-Entropy (BCE) Loss and SGD.
- **Visualization**: The script plots the original data points and the fitted logistic curve.

**Results**:
- The scatter plot shows the original data points colored by class.
- The red curve represents the sigmoid function output, which provides the probability of the input belonging to the positive class. This curve demonstrates the logistic regression model’s ability to separate the two classes.

---

### Classification

**Script**: `classification.py`

This script demonstrates binary classification using a simple neural network in PyTorch. The objective is to classify points into two classes based on input features.

- **Data generation**: The data is generated using the `make_moons` function from scikit-learn, which creates two interleaving half circles.
- **Model**: A neural network with one hidden layer and ReLU activations, followed by a sigmoid output layer.
- **Training**: The model is trained using Binary Cross-Entropy (BCE) Loss and the Adam optimizer.
- **Visualization**: The script plots the decision boundary along with the original data points.

**Results**:
- The contour plot shows the decision boundary learned by the model, highlighting regions where the model predicts class 0 or class 1.
- The scatter plot shows the original data points colored by class, illustrating how well the model has learned to separate the two interleaving half circles.

---

### K-Means Clustering

**Script**: `kmeans_clustering.py`

This script demonstrates clustering using the K-Means algorithm implemented with PyTorch. The objective is to group data points into clusters based on similarity.

- **Data generation**: The data is generated using the `make_blobs` function from scikit-learn, which creates clusters of points.
- **Model**: A simple implementation of K-Means clustering with centroids as trainable parameters.
- **Training**: The model is trained by minimizing the within-cluster sum of squares.
- **Visualization**: The script plots the clustered data points and centroids.

**Results**:
- The scatter plot shows the data points colored by their assigned cluster, demonstrating the model's ability to group similar points together.
- The red "X" marks represent the centroids of each cluster, indicating the central point of each group of data points. This helps to visually confirm the convergence of the K-Means algorithm and the effectiveness of the clustering process.

---

## Requirements

- Python 3.x
- PyTorch
- Matplotlib
- scikit-learn (for data generation in `classification.py` and `kmeans_clustering.py`)

## Usage

Run each script independently to see the respective machine learning task:

```bash
python linear_regression.py
python logistic_regression.py
python classification.py
python kmeans_clustering.py

