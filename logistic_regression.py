import torch
import torch.nn as nn
import torch.optim as optim
import matplotlib.pyplot as plt

# Generazione dei dati di esempio
torch.manual_seed(42)
x = torch.unsqueeze(torch.linspace(-10, 10, 100), dim=1)
y = ((2 * x + 1 + torch.randn(x.size()) * 2) > 0).float()  # Classi 0 e 1

# Visualizzazione dei dati di esempio
plt.scatter(x.numpy(), y.numpy(), label='Dati originali', c=y.numpy(), cmap='bwr')
plt.xlabel('X')
plt.ylabel('Y')
plt.legend()
plt.show()

# Definizione del modello di regressione logistica
class LogisticRegressionModel(nn.Module):
    def __init__(self):
        super(LogisticRegressionModel, self).__init__()
        self.linear = nn.Linear(1, 1)
    
    def forward(self, x):
        return torch.sigmoid(self.linear(x))

model = LogisticRegressionModel()

# Definizione della loss function e dell'ottimizzatore
criterion = nn.BCELoss()
optimizer = optim.SGD(model.parameters(), lr=0.01)

# Training del modello
num_epochs = 1000
for epoch in range(num_epochs):
    model.train()
    optimizer.zero_grad()
    outputs = model(x)
    loss = criterion(outputs, y)
    loss.backward()
    optimizer.step()

    if (epoch+1) % 100 == 0:
        print(f'Epoch [{epoch+1}/{num_epochs}], Loss: {loss.item():.4f}')

# Predizioni del modello addestrato
model.eval()
predicted = model(x).detach().numpy()

# Visualizzazione delle predizioni del modello
plt.scatter(x.numpy(), y.numpy(), label='Dati originali', c=y.numpy(), cmap='bwr')
plt.plot(x.numpy(), predicted, label='Fitted line', color='r')
plt.xlabel('X')
plt.ylabel('Probabilità')
plt.legend()
plt.show()

