import torch
import torch.nn as nn
import torch.optim as optim
import matplotlib.pyplot as plt

# Generazione di dati di esempio per la regressione lineare
# y = 2x + 1 con un po' di rumore
torch.manual_seed(42)
x = torch.unsqueeze(torch.linspace(-10, 10, 100), dim=1)
y = 2 * x + 1 + torch.randn(x.size()) * 2

# Visualizzazione dei dati di esempio
plt.scatter(x.numpy(), y.numpy(), label='Dati originali')
plt.xlabel('X')
plt.ylabel('Y')
plt.legend()
plt.show()

# Definizione del modello di regressione lineare
class LinearRegressionModel(nn.Module):
    def __init__(self):
        super(LinearRegressionModel, self).__init__()
        self.linear = nn.Linear(1, 1)
    
    def forward(self, x):
        return self.linear(x)

model = LinearRegressionModel()

# Definizione della loss function e dell'ottimizzatore
criterion = nn.MSELoss()
optimizer = optim.SGD(model.parameters(), lr=0.01)

# Training del modello
num_epochs = 1000
for epoch in range(num_epochs):
    model.train()
    optimizer.zero_grad()
    outputs = model(x)
    loss = criterion(outputs, y)
    loss.backward()
    optimizer.step()

    if (epoch+1) % 100 == 0:
        print(f'Epoch [{epoch+1}/{num_epochs}], Loss: {loss.item():.4f}')

# Predizioni del modello addestrato
model.eval()
predicted = model(x).detach().numpy()

# Visualizzazione delle predizioni del modello
plt.scatter(x.numpy(), y.numpy(), label='Dati originali')
plt.plot(x.numpy(), predicted, label='Fitted line', color='r')
plt.xlabel('X')
plt.ylabel('Y')
plt.legend()
plt.show()

# Stampa dei parametri del modello (peso e bias)
print(f'Peso (coeficiente angolare): {model.linear.weight.item()}')
print(f'Bias (intercetta): {model.linear.bias.item()}')

